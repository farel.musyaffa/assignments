package ddp2.tp4;

import java.util.*;

public class Algorithm {
    static String[] operators = new String[100];
    static int[] operatorsValue = new int[100];
    static String[] postfix = new String[100];
    static Stack<Long> evalStack = new Stack<Long>();
    public static String[] listOfErrors = new String[100];

    /**
     * Method untuk mengubah operasi dalam bentuk infix menjadi operasi postfix yang nantinya dievaluasi hasilnya
     * @param infix array yang berisi infix yang ingin diubah menjadi postfix
     * @return String array yang berisi postfix yang bisa dievaluasi
     */
    public static String[] toPostfix(String[] infix) {
        clearArray(operators);                          //Mengosongkan semua array yang akan dipakai
        clearArray(operatorsValue);                     //Agar hasil penghitungan dari operasi sebelumnya tidak ikut
        clearArray(postfix);
        clearArray(listOfErrors);
        while (!evalStack.empty()) {                    //Mengosongkan evalStack yang mungkin masih ada isi dari operasi sebelumnya
            evalStack.pop();
        }
        for (int i = 0; i < infix.length; i++) {
            if (infix[i] != null) {
                boolean isOperator = checkOperator(infix[i]);               //true jika infix pada index i berupa operator
                boolean isOperand = checkOperand(infix[i]);                 //true jika infix pada index i berupa operand
                boolean isCloseBracket = (infix[i].equals(")"));            //true jika infix pada index i berupa tutup kurung
                boolean isOpenBracket = (infix[i].equals("("));             //true jika infix pada index i berupa buka kurung

                if (isOpenBracket) {                                        //jika infix berupa buka kurung:
                    addToArray(infix[i], operators);                        //tambahkan ke array operator,
                    int value = getOperatorValue(infix[i]);                 //set nilai operator,
                    addToArray(value, operatorsValue);                      //dan tambahkan ke array operatorValue.

                } else if (isOperator) {                                    //jika infix berupa operator:
                    int value = getOperatorValue(infix[i]);                 //set nilai operator,
                    popOperators(value);                                    //pop semua operator hingga habis, ada open bracket, atau ada operator yang valuenya lebih kecil
                    addToArray(infix[i], operators);                        //add operator baru ke array operator
                    addToArray(value, operatorsValue);                      //beserta valuenya ke array operatorValue

                } else if (isOperand) {                                     //jika infix berupa operand:
                    addToArray(infix[i], postfix);                          //langsung add ke postfix

                } else if (isCloseBracket) {                                //jika infix berupa tutup kurung:
                    boolean hasOpenBracket = checkOpenBracket();            //cek apakah ada open bracket yang bisa dipasangkan dengan close bracket
                    if (!hasOpenBracket) {                                              
                        addToArray("Missing open paranthesis", listOfErrors);       //add ke listoferrors bahwa tidak ada pasangan dari close bracket
                        popOperators(-1);                                           //pop semua operator hingga habis atau ada operator yang valuenya lebih kecil
                    } else {
                        popOperators(-1);                                           //pop semua operator sesuai ketentuan
                        int indexOfOpenBracket = getLastArrayIndex(operators);      
                        operators[indexOfOpenBracket] = null;                       //hapus operator open bracket dari array operators
                        operatorsValue[indexOfOpenBracket] = 0;                     //beserta valuenya
                    }
                    
                } else {                                                    //kalau bukan keempat-empatnya
                    addToArray(infix[i], postfix);                          //anggap sebagai operand(langsung dimasukkan ke postfix)
                    addToArray("Invalid operand", listOfErrors);            //add error "invalid operand" pada listoferrors

                }
            }
        }
        boolean hasOpenBracket = checkOpenBracket();                        //jika setelah perubahan ke postfix sesesai dilakukan dan masih ada open bracket di array operators,
        if (hasOpenBracket) {                                               //berarti ada satu atau lebih close bracket yang kurang dari infix
            addToArray("Missing one or more close bracket", listOfErrors);
        }
        popAllOperators();
        return postfix;
    }

    /**
     * Evaluasi nilai dari postfix yang telah dihasilkan.
     * @param postfix = operasi yang ingin dilakukan, dalam bentuk postfix.
     * @return nilai hasil evaluasi operasi
     */
    public static long postfixEvaluation(String[] postfix) {
        for (int i = 0; i < getLastArrayIndex(postfix) + 1; i++) {
            boolean isOperand = checkOperand(postfix[i]);               //true jika postfix[i] berupa operand
            boolean isOperator = checkOperator(postfix[i]);             //true jika postfix[i] berupa operator

            if (isOperand) {
                long operand = Long.parseLong(postfix[i]);              //ubah operand dari string menjadi long
                evalStack.push(operand);                                //push ke stack
            
            } else if (isOperator) {
                evaluate(postfix[i]);                                   //operasikan dua nilai teratas di stack dengan operator (postfix[i])
            
            }
        }
        if (evalStack.isEmpty()) {                                      //jika evalstack kosong (karena error atau operand yang kurang), return 0
            return 0;
        } else {                                                        //jika tidak, objek teratas di evalStack seharusnya merupakan hasil akhir.
            return evalStack.pop();                                     //pop dan return hasil akhir.
        }
    }

    /**
     * Digunakan untuk melaksanakan operasi antara 2 objek teratas pada stack dengan operator yang dispesifikan pada parameter.
     * @param operator = menentukan operasi yang akan dilakukan
     */
    private static void evaluate(String operator) {
        boolean validOperation = true;
        long y = 0;                                                 
        long x = 0;
        try {                                                           //ambil objek teratas pada stack dan assign ke y.
            y = evalStack.pop();
        } catch (EmptyStackException e) {                               //jika stack ternyata kosong, berarti ada operand yang kurang dari postfix.
            addToArray("Missing operand", listOfErrors);
            validOperation = false;
        }

        try {
            x = evalStack.pop();
        } catch (EmptyStackException e) {
            addToArray("Missing operand", listOfErrors);
            evalStack.push(y);                                          //push kembali y jika pop pertama tidak bermasalah tetapi pop kedua bermasalah
            validOperation = false;
        }

        if (validOperation) {
            switch (operator) {
                case "+":
                    evalStack.push(x+y);
                    break;
                case "-":
                    evalStack.push(x-y);
                    break;
                case "*":
                    evalStack.push(x*y);
                    break;
                case "/":
                    try {
                        evalStack.push(x/y);
                    } catch (ArithmeticException e) {
                        addToArray("Division by zero", listOfErrors);
                        evalStack.push(x);
                        evalStack.push(y);
                    }
                    break;
                case "^":
                    Double temp = Math.pow(x, y);
                    x = temp.longValue();               //karena Math.pow() akan menghasilkan double, ubah terlebih dahulu hasil pemangkatan menjadi long
                    evalStack.push(x);
            }
        }
    }

    /**
     * Pop semua operator yang ada pada array operators. Pop adalah memindahkan semua operator dalam array ke postfix dan mendelete operator dalam array operators tersebut.
     */
    private static void popAllOperators() {
        for (int i = getLastArrayIndex(operators); i >= 0; i--) {
            if (operators[i] != null && !(operators[i].equals("(") || operators[i].equals(")"))) {      
                for (int j = 0; j < postfix.length; j++) {
                    if (postfix[j] == null) {                   //cari index kosong pada postfix
                        postfix[j] = operators[i];              //pindahkan operator dari array operators ke postfix.
                        operators[i] = null;
                        operatorsValue[i] = 0;
                        break;
                    }
                }
            }
        }
    }

    /**
     * Pop operator yang ada pada array operators hingga ada operator yang valuenya lebih kecil, ada buka kurung, atau habis
     * @param input = value dari operator yang baru
     */
    private static void popOperators(int input) {
        for (int i = getLastArrayIndex(operators); i >= 0; i--) {
            if (operators[i].equals("(")) {                                     //jika sampai ke buka kurung, jangan pop operator yang ada di luar kurung
                break;
            } else if (operators[i] != null && !(operators[i].equals("(") || operators[i].equals(")"))) {
                boolean nextIsLesser = checkLesserOperator(input, i);           //true jika nilai operator yang dicek lebih kecil daripada nilai operator yang di push.
                if (nextIsLesser) {
                    for (int j = 0; j < postfix.length; j++) {
                        if (postfix[j] == null) {
                            postfix[j] = operators[i];
                            operators[i] = null;
                            operatorsValue[i] = 0;
                            break;
                        }
                    }
                }
            }
        }
    }

    /**
     * Cek apakah infix berupa operator.
     * @param input = infix yang dicek
     * @return true jika infix berupa operator, false jika bukan
     */
    private static boolean checkOperator(String input) {
        String[] listOfOperators = {"+", "-", "*", "/", "^"};
        for (int i = 0; i < listOfOperators.length; i++) {
            if (listOfOperators[i].equals(input)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Cek apakah infix berupa operand.
     * @param input = infix yang ingin dicek
     * @return true jika berupa operand, false jika bukan
     */
    private static boolean checkOperand(String input) {
        try {
            Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    /**
     * Mengecek value dari operator. + dan - : 1, * dan / : 2, ^ : 3, ( : -1.
     * @param input = operator yang ingin didapatkan valuenya
     * @return value dari operator tersebut
     */
    private static int getOperatorValue(String input) {
        switch (input) {
            case "+":
            case "-":
                return 1;
            case "*":
            case "/":
                return 2;
            case "^":
                return 3;
            case "(":
                return -1;
            default:
                return 0;
        }
    }

    /**
     * Mencari index valid yang paling ujung pada suatu array.
     * @param array = array yang ingin dicek
     * @return index
     */
    private static int getLastArrayIndex(String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                return --i;
            }
        }
        return -1;
    }

    /**
     * Mengecek apakah ada open bracket pada array operators.
     * @return true jika ada open bracket.
     */
    private static boolean checkOpenBracket() {
        for (int i = 0; i < operators.length; i++) {
            if (operators[i] != null && operators[i].equals("(")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Mengecek apakah nilai operator baru lebih kecil daripada nilai operator pada index yang ditunjuk.
     * @param value = nilai operator baru
     * @param indexOfOperator = index dari operator yang ditunjuk (operator yang ingin dicek)
     * @return true jika operator yang baru memiliki nilai lebih kecil atau berlaku sesuai ketentuan ketika nilai sama.
     */
    private static boolean checkLesserOperator(int value, int indexOfOperator) {
        if (value < operatorsValue[indexOfOperator]) {
            return true;
        } else if (value == operatorsValue[indexOfOperator] && value != 3) {
            return true;
        } else if (value == operatorsValue[indexOfOperator] && value == 3) {
            return false;
        }
        return false;
    }

    /**
     * Menambahkan suatu objek kedalam array.
     * @param input = objek yang ingin ditambahkan
     * @param array = array yang ingin ditambahkan dengan objek
     */
    private static void addToArray(String input, String[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == null) {
                array[i] = input;
                break;
            }
        }
    }

    /**
     * Menambahkan suatu objek kedalam array.
     * @param input = objek yang ingin ditambahkan
     * @param array = array yang ingin ditambahkan dengan objek
     */
    private static void addToArray(int input, int[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                array[i] = input;
                break;
            }
        }
    }
    
    /**
     * Menghapus semua isi dalam suatu array
     * @param array = array yang ingin diclear
     */
    private static void clearArray(String[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = null;
        }
    }

    /**
     * Menghapus semua isi dalam suatu array
     * @param array = array yang ingin diclear
     */
    private static void clearArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = 0;
        }
    }
}