package ddp2.tp4;

import java.util.*;
import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Infix to Postfix Evaluator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        EvaluatorPanel evaluatorPanel = new EvaluatorPanel();
        frame.setResizable(false);
        frame.add(evaluatorPanel);
        frame.pack();
        frame.setVisible(true);
    }
}
