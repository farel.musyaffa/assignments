package ddp2.tp4;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.StringTokenizer;

public class EvaluatorPanel extends JPanel {
    private JTextField infix;
    private JLabel infixLabel;
    private JLabel postfixLabel;
    private JLabel resultLabel;
    private JLabel errorLabel;
    private JTextArea postfix;
    private JTextArea result;
    private JTextArea error;
    public String input;

    public EvaluatorPanel() {
        SpringLayout layout = new SpringLayout();                       //memakai sistem layout SpringLayout
        setLayout(layout);
        infixLabel = new JLabel("Infix expression : ");
        infix = new JTextField(40);
        postfixLabel = new JLabel("Postfix expression : ");
        postfix = new JTextArea(1, 40);
        resultLabel = new JLabel("Result : ");
        result = new JTextArea(1, 40);
        errorLabel = new JLabel("Error Messages : ");
        error = new JTextArea(1, 40);
        infix.addActionListener(new EnterListener());
        add(infixLabel);
        add(infix);
        add(postfixLabel);
        add(postfix);
        add(resultLabel);
        add(result);
        add(errorLabel);
        add(error);

        //Setting posisi tiap komponen
        layout.putConstraint(SpringLayout.WEST, infixLabel, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, infixLabel, 5, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.WEST, infix, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, infix, 5, SpringLayout.SOUTH, infixLabel);

        layout.putConstraint(SpringLayout.WEST, postfixLabel, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, postfixLabel, 5, SpringLayout.SOUTH, infix);
        layout.putConstraint(SpringLayout.WEST, postfix, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, postfix, 5, SpringLayout.SOUTH, postfixLabel);

        layout.putConstraint(SpringLayout.WEST, resultLabel, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, resultLabel, 5, SpringLayout.SOUTH, postfix);
        layout.putConstraint(SpringLayout.WEST, result, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, result, 5, SpringLayout.SOUTH, resultLabel);

        layout.putConstraint(SpringLayout.WEST, errorLabel, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, errorLabel, 5, SpringLayout.SOUTH, result);
        layout.putConstraint(SpringLayout.WEST, error, 10, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, error, 5, SpringLayout.SOUTH, errorLabel);

        //setting size dari window
        layout.putConstraint(SpringLayout.EAST, this, 10, SpringLayout.EAST, infix);
        layout.putConstraint(SpringLayout.SOUTH, this, 10, SpringLayout.SOUTH, error);

    }

    private class EnterListener implements ActionListener {
        public void actionPerformed (ActionEvent e) {                                   //Mengeksekusi actionPerformed ketika enter ditekan
            input = infix.getText();                                                    //Mengambil text dari textfield infix
            StringTokenizer token = new StringTokenizer(input, "+-*/^() ", true);       
            String[] operation = new String[1000];
            while (token.hasMoreTokens()) {                                             //Mengubah string infix menjadi string yang telah diubah berdasarkan tokenizer
                String next = token.nextToken();                                        //Lalu memasukkan tiap token ke array baru
                if (!next.equals(" ")) {                                                //Token yang berupa " " kosong tidak dimasukkan ke array
                    for (int i = 0; i < operation.length; i++) {
                        if (operation[i] == null) {
                            operation[i] = next;
                            break;                                                          
                        }
                    }
                }
            }
            String[] algorithmPostfixResult = Algorithm.toPostfix(operation);           //Memanggil algoritma topostfix, return hasil postfix dalam bentuk string array
            String postfixString = "";
            String algorithmError = "";
            for (int i = 0; i < algorithmPostfixResult.length; i++) {                   //menggabungkan tiap objek pada array postfix menjadi satu string
                if (algorithmPostfixResult[i] != null) {
                    postfixString += algorithmPostfixResult[i] + " ";
                }
            }

            String algorithmResult = Long.toString(Algorithm.postfixEvaluation(algorithmPostfixResult));        //Mengevaluasi string postfix yang tadi telah didapatkan, dan mengubah hasil menjadi string agar bisa dimasukkan ke textarea
            for (int i = 0; i < Algorithm.listOfErrors.length; i++) {
                if (Algorithm.listOfErrors[i] != null) {
                    if (algorithmError.length() == 0) {
                        algorithmError += Algorithm.listOfErrors[i];                                         //menggabungkan listOfErrors menjadi satu string
                    } else {
                        algorithmError += ", " + Algorithm.listOfErrors[i];
                    }
                }
            }

            postfix.setText(postfixString);
            result.setText(algorithmResult);
            error.setText(algorithmError);
        }
    }
}
