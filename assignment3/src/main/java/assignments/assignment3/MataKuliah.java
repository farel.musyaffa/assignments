package assignments.assignment3;

public class MataKuliah {
    private String nama;
    
    private int kapasitas;

    private Dosen dosen;

    private Mahasiswa[] daftarMahasiswa;

    /** Constructor kelas MataKuliah. Besarnya array daftarMahasiswa bergantung terhadap kapasitas mata kuliah. size daftarMahasiswa == kapasitas
     * @param nama = nama mata kuliah
     * @param kapasitas = kapasitas mata kuliah, menentukan size array daftarMataKuliah
     */
    public MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    /** Menambahkan objek mahasiswa ke daftarMahasiswa.
     * 
     * @param mahasiswa = mahasiswa yang akan ditambahkan ke daftarMahasiswa
     */
    public void addMahasiswa(Mahasiswa mahasiswa) {
        if (canAddMahasiswa()) {
            for (int i = 0; i < daftarMahasiswa.length; i++) {
                if (daftarMahasiswa[i] == null) {                                                   //daftarMahasiswa[i] == null -> index kosong, bisa diisi oleh suatu objek
                    daftarMahasiswa[i] = mahasiswa;
                    break;
                }
            }
        } else {
            System.out.println("[Ditolak] Kapasitas matkul sudah penuh");
        }
    }

    //** Drop mahasiswa dari mata kuliah. Cari objek mahasiswa pada array daftarMahasiswa, dan ubah index tersebut menjadi null */
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] != null && daftarMahasiswa[i] == mahasiswa) {
                daftarMahasiswa[i] = null;
            }
        }
    }

    /** Menambahkan dosen untuk mengajar mata kuliah
     * @param dosen = dosen yang akan mengajar mata kuliah
     */
    public void addDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    /** Drop dosen yang sedang mengajar. Variabel dosen diubah menjadi null.*/
    public void dropDosen() {
        this.dosen = null;
    }

    /** Menentukan apakah mata kuliah bisa menambahkan dosen. Jika ada objek dosen yang sedang mengajar mata kuliah, mata kuliah tidak bisa menambahkan dosen baru
     * @return true jika dosen == null, false jika dosen != null
     */
    public boolean canAddDosen() {
        if (dosen == null) {
            return true;
        } else {
            return false;
        }
    }

    /** Jika jumlah mahasiswa == kapasitas, maka mata kuliah sudah penuh, dan tidak bisa menambahkan mahasiswa baru
     * @return true jika kapasitas sudah penuh, false jika masih ada tempat kosong
     */
    public boolean canAddMahasiswa() {
        int count = getJumlahMahasiswa();
        if (count < kapasitas) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return this.nama;
    }

    /** Menghitung jumlah mahasiswa yang mengambil mata kuliah ini.
     * 
     * @return jumlah objek mahasiswa pada daftarMahasiswa
     */
    public int getJumlahMahasiswa() {
        int count = 0;
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] != null) {
                count++;
            }
        }
        return count;
    }

    /** @return kapasitas mata kuliah */
    public int getKapasitas() {
        return kapasitas;
    }

    /** @return - String "Belum ada" jika belum ada dosen pengajar, nama dosen jika sudah ada dosen pengajar*/
    public String getDosen() {
        if (dosen == null) {
            return "Belum ada";
        } else {
            return dosen.toString();
        }
    }

    /** @return daftar mahasiswa */
    public Mahasiswa[] getDaftarMahasiswa() {
        return daftarMahasiswa;
    }
}