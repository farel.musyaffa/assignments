package assignments.assignment3;

import java.text.spi.DateFormatProvider;
import java.util.Scanner;

public class Main {
    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];

    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];

    private static int totalMataKuliah = 0;

    private static int totalElemenFasilkom = 0;

    /** Membuat objek Mahasiswa dan menambahkan objek tersebut pada aray daftarElemenFasilkom.
     * 
     * @param nama = nama mahasiswa
     * @param npm = npm mahasiswa
     */
    private static void addMahasiswa(String nama, long npm) {
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] == null) {
                daftarElemenFasilkom[i] = new Mahasiswa(nama, npm);
                totalElemenFasilkom++;
                break;
            }
        }
        System.out.println(nama + " berhasil ditambahkan");
    }

    /** Membuat objek baru Dosen dan menambahkan objek tersebut pada daftarElemenFasilkom.
     * 
     * @param nama = nama dosen
     */
    private static void addDosen(String nama) {
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] == null) {
                daftarElemenFasilkom[i] = new Dosen(nama);
                totalElemenFasilkom++;
                break;
            }
        }
        System.out.println(nama + " berhasil ditambahkan");
    }

    /** Membuat objek ElemenKantin baru dan menambahkan objek tersebut pada daftarElemenFasilkom.
     * 
     * @param nama = nama elemen kantin
     */
    private static void addElemenKantin(String nama) {
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] == null) {
                daftarElemenFasilkom[i] = new ElemenKantin(nama);
                totalElemenFasilkom++;
                break;
            }
        }
        System.out.println(nama + " berhasil ditambahkan");
    }

    /** Mencari objek dengan nama objek1 dan objek2 pada daftarElemenFasilkom dan memanggil method menyapa dari class ElemenFasilkom.
     * Serta menghitung friendship ketika dosen memiliki mata kuliah yang sama dengan mahasiswa yang menyapa.
     * @param objek1 = nama objek pertama
     * @param objek2 = nama objek kedua
     */
    private static void menyapa(String objek1, String objek2) {
        if (objek1.equals(objek2)) {
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        } else {
            ElemenFasilkom target1 = searchElemen(objek1);                                              //Mereturn objek ElemenFasilkom dengan nama objek1
            ElemenFasilkom target2 = searchElemen(objek2);                                              //Mereturn objek ElemenFasilkom dengan nama objek2
            target1.menyapa(target2);                                                                   
            if (target1.getTipe().equals("Dosen") && target2.getTipe().equals("Mahasiswa")) {   
                Mahasiswa mahasiswa = (Mahasiswa)target2;                                               //Casting kedua objek menjadi Mahasiswa dan dosen agar bisa dicek mata kuliah sama atau tidak
                Dosen dosen = (Dosen)target1;
                if (mahasiswa.cekMatkul(dosen.getMataKuliah())) {
                    target1.setFriendship(2);
                    target2.setFriendship(2);
                }
            } else if (target1.getTipe().equals("Mahasiswa") && target2.getTipe().equals("Dosen")) {
                Mahasiswa mahasiswa = (Mahasiswa)target1;
                Dosen dosen = (Dosen)target2;
                if (mahasiswa.cekMatkul(dosen.getMataKuliah())) {
                    target1.setFriendship(2);
                    target2.setFriendship(2);
                }
            }
        }
    }

    /** Membuat objek Makanan baru dan menambahkan objek tersebut ke dalam array daftarMakanan apabila makanan tersebut belum ada di daftar.
     * 
     * @param objek = nama elemen kantin yang akan menambahkan makanan ke dalam daftar makanan
     * @param namaMakanan = nama makanan yang akan ditambahkan
     * @param harga = harga makanan yang akan ditambahkan
     */
    private static void addMakanan(String objek, String namaMakanan, long harga) {
        ElemenFasilkom elemen = searchElemen(objek);
        if (elemen.getTipe().equals("ElemenKantin")) {
            ((ElemenKantin)elemen).setMakanan(namaMakanan, harga);
        } else {
            System.out.println("[DITOLAK] " + objek + " bukan merupakan elemen kantin");
        }
    }

    /** Melakukan interaksi membeli makanan antar ElemenFasilkom dengan ElemenKantin.
     * Memanggil method membeliMakanan dari ElemenFasilkom.
     * @param objek1 = nama pembeli. Pembeli harus beda dengan penjual.
     * @param objek2 = nama penjual, harus bertipe "ElemenKantin".
     * @param namaMakanan = nama makanan yang akan dibeli.
     */
    private static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        ElemenFasilkom target1 = searchElemen(objek1);
        ElemenFasilkom target2 = searchElemen(objek2);
        if (target1 == target2) {
            System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
        } else if (target2.getTipe().equals("ElemenKantin")) {
            ElemenFasilkom.membeliMakanan(target1, target2, namaMakanan);
        } else {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
    }

    /** Membuat objek MataKuliah baru dan menambahkan objek tersebut ke daftarMataKuliah.
     * 
     * @param nama = nama mata kuliah
     * @param kapasitas = kapasitas mata kuliah
     */
    private static void createMatkul(String nama, int kapasitas) {
        MataKuliah matkul = new MataKuliah(nama, kapasitas);
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] == null) {
                daftarMataKuliah[i] = matkul;
                totalMataKuliah++;
                break;
            }
        }
        System.out.println(nama + " berhasil ditambahkan dengan kapasitas " + kapasitas);
    }

    /** Mencari matkul yang akan ditambahkan oleh mahasiswa di daftarMataKuliah dan menambahkan objek tersebut ke daftarMataKuliah milih mahasiswa.
     * 
     * @param objek = nama mahasiswa yang akan menambahkan mata kuliah. Harus bertipe "Mahasiswa".
     * @param namaMataKuliah = nama mata kuliah yang akan ditambahkan.
     */
    private static void addMatkul(String objek, String namaMataKuliah) {
        if (searchElemen(objek).getTipe().equals("Mahasiswa")) {
            Mahasiswa target = (Mahasiswa)searchElemen(objek);
            MataKuliah matkul = searchMatkul(namaMataKuliah);
            target.addMatkul(matkul);
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }
    }

    /** Drop matkul yang diambil oleh mahasiswa. Objek MataKuliah diubah menjadi null pada daftarMataKuliah di mahasiswa dan begitu pula sebaliknya.
     * 
     * @param objek = nama mahasiswa. Harus bertipe "Mahasiswa".
     * @param namaMataKuliah = nama mata kuliah yang akan didrop.
     */
    private static void dropMatkul(String objek, String namaMataKuliah) {
        if (searchElemen(objek).getTipe().equals("Mahasiswa")) {
            Mahasiswa target = (Mahasiswa)searchElemen(objek);
            MataKuliah matkul = searchMatkul(namaMataKuliah);
            target.dropMatkul(matkul);
        } else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    /** Mengadd objek dosen yang belum mengajar mata kuliah ke suatu mata kuliah yang belum memiliki dosen pengajar.
     * 
     * @param objek = nama dosen. Harus bertipe "Dosen".
     * @param namaMataKuliah = nama mata kuliah.
     */
    private static void mengajarMatkul(String objek, String namaMataKuliah) {
        if (searchElemen(objek).getTipe().equals("Dosen")) {
            Dosen dosen = (Dosen)searchElemen(objek);
            MataKuliah matkul = searchMatkul(namaMataKuliah);
            dosen.mengajarMataKuliah(matkul);
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }
    }

    /** Menghapus objek MataKuliah dari objek dosen dan begitu pula sebaliknya.
     * 
     * @param objek = nama dosen yang akan berhenti mengajar mata kuliah yang ia ajar.
     */
    private static void berhentiMengajar(String objek) {
        if (searchElemen(objek).getTipe().equals("Dosen")) {
            Dosen dosen = (Dosen)searchElemen(objek);
            dosen.dropMataKuliah();
        } else {
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }

    /** Memprint ringkasan informasi mahasiswa.
     * 
     * @param objek = mahasiswa yang akan diprint ringkasannya. Harus bertipe "Mahasiswa";
     */
    private static void ringkasanMahasiswa(String objek) {
        if (searchElemen(objek).getTipe().equals("Mahasiswa")) {
            Mahasiswa mahasiswa = (Mahasiswa)searchElemen(objek);
            System.out.println("Nama: " + mahasiswa);
            System.out.println("Tanggal lahir: " + mahasiswa.extractTanggalLahir(mahasiswa.getNPM()));
            System.out.println("Jurusan: " + mahasiswa.extractJurusan(mahasiswa.getNPM()));
            System.out.println("Daftar Mata Kuliah:");
            if (mahasiswa.getJumlahMatkul() == 0) {
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                for (int i = 0, j = 1; i < mahasiswa.getDaftarMatkul().length; i++) {
                    if (mahasiswa.getDaftarMatkul()[i] != null) {
                        System.out.println(j++ + ". " + mahasiswa.getDaftarMatkul()[i]);
                    }
                }
            }
        } else {
            System.out.println("[DITOLAK] " + objek + " bukan merupakan seorang mahasiswa");
        }
    }

    /** Memprint ringakasan mata kuliah.
     * 
     * @param namaMataKuliah = nama mata kuliah yang ingin diprint ringkasannya.
     */
    private static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah matkul = searchMatkul(namaMataKuliah);
        System.out.println("Nama mata kuliah: " + matkul);
        System.out.println("Jumlah mahasiswa: " + matkul.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + matkul.getKapasitas());
        System.out.println("Dosen pengajar: " + matkul.getDosen());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
        if (matkul.getJumlahMahasiswa() == 0) {
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            for (int i = 0, j = 1; i < matkul.getDaftarMahasiswa().length; i++) {
                if (matkul.getDaftarMahasiswa()[i] != null) {
                    System.out.println(j++ + ". " + matkul.getDaftarMahasiswa()[i]);
                }
            }
        }
    }

    /** Menghitung friendship rating dari tiap ElemenFasilkom dalam daftarElemenFasilkom dan mereset array telahMenyapa.
     * Print ranking friendship dari ElemenFasilkom dengan memanggil method friendshipRanking
    */
    private static void nextDay() {
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] != null) {
                int friendshipChange = 0;
                int jumlahMenyapa = daftarElemenFasilkom[i].countMenyapa();
                if (jumlahMenyapa >= totalElemenFasilkom/2) {
                    friendshipChange = 10;
                } else {
                    friendshipChange = -5;
                }
                daftarElemenFasilkom[i].setFriendship(friendshipChange);
                daftarElemenFasilkom[i].resetMenyapa();
            }
        }
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    /** Mengatur urutan ranking dari ElemenFasilkom berdasarkan friendship rating dan alfabet. */
    private static void friendshipRanking() {
        for (int i = 0; i < daftarElemenFasilkom.length - 1; i++) {
            for (int j = i+1; j < daftarElemenFasilkom.length; j++) {
                if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[j] != null && daftarElemenFasilkom[i].getFriendship() < daftarElemenFasilkom[j].getFriendship()) {
                    //Jika objek pertama yang dicek memiliki friendship rating yang lebih rendah dengan objek kedua
                    //Tukar posisi
                    ElemenFasilkom temp = daftarElemenFasilkom[i];
                    daftarElemenFasilkom[i] = daftarElemenFasilkom[j];
                    daftarElemenFasilkom[j] = temp;
                } else if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[j] != null && daftarElemenFasilkom[i].getFriendship() == daftarElemenFasilkom[j].getFriendship()) {
                    //Jika friendship rating antar kedua objek sama,
                    if (daftarElemenFasilkom[i] != null && daftarElemenFasilkom[j] != null && daftarElemenFasilkom[i].toString().compareTo(daftarElemenFasilkom[j].toString()) > 0) {
                        //Urut secara alfabetis. Method compareTo akan mereturn positif jika string lebih duluan secara alfabetis dibandingkan dengan string yang dicompare.
                        ElemenFasilkom temp = daftarElemenFasilkom[i];
                        daftarElemenFasilkom[i] = daftarElemenFasilkom[j];
                        daftarElemenFasilkom[j] = temp;
                    }
                }
            }
        }
        for (int i = 0, j = 1; i < daftarElemenFasilkom.length; i++) {
            //print ranking
            if (daftarElemenFasilkom[i] != null) {
                System.out.println(j++ + ". " + daftarElemenFasilkom[i] + "(" + daftarElemenFasilkom[i].getFriendship() + ")");
            }
        }
    }

    /** Mengakhiri program dan memprint ranking terakhir. */
    private static void programEnd() {
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        friendshipRanking();
    }

    /** Mencari objek ElemenFasilkom pada daftarElemenFasilkom dan mereturn objek tersebut.
     * 
     * @param nama = nama objek yang dicari
     * @return objek ElemenFasilkom dengan nama yang sama
     */
    private static ElemenFasilkom searchElemen(String nama) {
        ElemenFasilkom elemen = null;
        for (int i = 0; i < daftarElemenFasilkom.length; i++) {
            if (daftarElemenFasilkom[i] != null && nama.equals(daftarElemenFasilkom[i].toString())) {
                elemen = daftarElemenFasilkom[i];
                break;
            }
        }
        return elemen;
    }

    /** Mencari objek MataKuliah yang ingin dicari pada daftarMataKuliah dan mereturn objek tersebut.
     * 
     * @param namaMataKuliah = nama mata kuliah yang dicari
     * @return objek MataKuliah
     */
    private static MataKuliah searchMatkul(String namaMataKuliah) {
        MataKuliah matkul = null;
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] != null && namaMataKuliah.equals(daftarMataKuliah[i].toString())) {
                matkul = daftarMataKuliah[i];
                break;
            }
        } 
        return matkul;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
        input.close();
    }
}