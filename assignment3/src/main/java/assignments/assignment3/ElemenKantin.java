package assignments.assignment3;

public class ElemenKantin extends ElemenFasilkom {
    private Makanan[] daftarMakanan = new Makanan[10];

    /** Constructor ElemenKantin. Tipe = "ElemenKantin" */
    public ElemenKantin(String nama) {
        super(nama, "ElemenKantin");
    }

    /** Menambahkan makanan baru pada daftarMakanan. Membuat objek Makanan baru, jika objek Makanan belum ada di daftarMakanan, add objek ke dalam array daftarMakanan
     * 
     * @param nama = nama makanan yang akan ditambahkan
     * @param harga = harga makanan yang akan ditambahkan
     */
    public void setMakanan(String nama, long harga) {
        Makanan food = new Makanan(nama, harga);
        boolean found = false;
        for (int i = 0; i < daftarMakanan.length; i++) {                                        //Cari objek Makanan pada array daftarMakanan
            if (daftarMakanan[i] == food) {                                                     //daftarMakanan[i] == food -> sudah ada objek Makanan di daftarMakanan
                found = true;                   
                System.out.println("[DITOLAK] " + food.toString() + "sudah pernah terdaftar");
                break;
            }
        }
        if (!found) {                                                                           //found == false jika objek Makanan belum ada di daftarMakanan
            for (int i = 0; i < daftarMakanan.length; i++) {
                if (daftarMakanan[i] == null) {
                    daftarMakanan[i] = food;
                    System.out.println(this.nama + " telah mendaftarkan makanan " + nama + " dengan harga " + harga);
                    break;
                }
            }
        }
    }

    /** @return array daftarMakanan */
    public Makanan[] getDaftarMakanan() {
        return daftarMakanan;
    }

    @Override
    public String toString() {
        return nama;
    }
}