package assignments.assignment3;

public class Mahasiswa extends ElemenFasilkom {
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    
    private long npm;

    private String tanggalLahir;
    
    private String jurusan;

    /** Constructor class Mahasiswa. Tipe -> "Mahasiswa" */
    public Mahasiswa(String nama, long npm) {
        super(nama, "Mahasiswa");
        this.npm = npm;
    }

    /** Menambahkan objek mataKuliah ke array daftarMataKuliah, dan menambahkan objek Mahasiswa ke array daftarMahasiswa pada objek mataKuliah.
     * Mahasiswa harus belum mengambil mata kuliah, mata kuliah harus memiliki sisa kapasitas
     * @param mataKuliah = objek mataKuliah yang ingin ditambah
     */
    public void addMatkul(MataKuliah mataKuliah) {
        if (!cekMatkul(mataKuliah) && mataKuliah.canAddMahasiswa()) {
            for (int i = 0; i < daftarMataKuliah.length; i++) {
                if (daftarMataKuliah[i] == null) {
                    daftarMataKuliah[i] = mataKuliah;
                    mataKuliah.addMahasiswa(this);
                    System.out.println(nama + " berhasil menambahkan mata kuliah " + mataKuliah);
                    break;
                }
            }
        } else if (cekMatkul(mataKuliah)){
            System.out.println("[DITOLAK] " + mataKuliah + " telah diambil sebelumnya");
        } else if (!mataKuliah.canAddMahasiswa()) {
            System.out.println("[DITOLAK] " + mataKuliah + " telah penuh kapasitasnya");
        }
    }

    /** Mengubah objek mataKuliah pada array daftarMataKuliah menjadi null,
     * serta mengubah objek mahasiswa pada array daftarMahasiswa pada objek mataKuliah menjadi null.
     * @param mataKuliah = mata kuliah yang akan di drop oleh mahasiswa
     */
    public void dropMatkul(MataKuliah mataKuliah) {
        if (cekMatkul(mataKuliah)) {
            for (int i = 0; i < daftarMataKuliah.length; i++) {
                if (daftarMataKuliah[i] == mataKuliah) {
                    daftarMataKuliah[i] = null;
                    mataKuliah.dropMahasiswa(this);
                    System.out.println(this + " berhasil drop mata kuliah " + mataKuliah);
                }
            }
        } else {
            System.out.println("[DITOLAK] " + mataKuliah + " belum pernah diambil");
        }
    }

    /** Mengubah long npm menjadi String, dan mengekstraksi informasi tanggal lahir dari npm menggunakan substring
     * @param npm = npm yang akan digunakan untuk menentukan tanggal lahir
     * @return String berupa tanggal lahir
     */
    public String extractTanggalLahir(long npm) {
        String tanggalLahir = "" + npm;
        String tanggal;
        String bulan;
        String tahun = tanggalLahir.substring(8, 12);
        if (tanggalLahir.substring(4, 5).equals("0")) {
            tanggal = tanggalLahir.substring(5, 6);
        } else {
            tanggal = tanggalLahir.substring(4, 6);
        }
        if (tanggalLahir.substring(6,7).equals("0")) {
            bulan = tanggalLahir.substring(7, 8);
        } else {
            bulan = tanggalLahir.substring(6, 8);
        }
        return tanggal + "-" + bulan + "-" + tahun;
    }

    /** Mengubah long npm menjadi string, dan mengekstraksi informasi jurusan dari npm menggunakan substring
     * @param npm = npm yang akan digunakan untuk menentukan jurusan
     * @return String berupa jurusan mahasiswa
     */
    public String extractJurusan(long npm) {
        jurusan = "" + npm;
        String res = "";
        switch (Integer.parseInt(jurusan.substring(2,4))) {
            case 01 -> res = "Ilmu Komputer";
            case 02 -> res = "Sistem Informasi";
        }
        return res;
    }

    /** Mengecek apakah objek mataKuliah di parameter sudah ada di daftarMataKuliah mahasiswa.
     * 
     * @param mataKuliah = mata kuliah yang akan dicek di daftarMataKuliah
     * @return true jika sudah ada mata kuliah di daftar, false jika belum
     */
    public boolean cekMatkul(MataKuliah mataKuliah) {
        boolean found = false;
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i] == mataKuliah) {
                found = true;
            }
        }
        return found;
    }

    /** Menghitung jumlah mata kuliah yang sudah diambil oleh mahasiswa.
     * 
     * @return jumlah objek mataKuliah pada array daftarMataKuliah
     */
    public int getJumlahMatkul() {
        int count = 0;
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] != null) {
                count++;
            }
        }
        return count;
    }

    @Override
    public String toString() {
        return nama;
    }

    /** @return npm mahasiswa */
    public long getNPM() {
        return npm;
    }

    /** @return daftarMataKuliah mahasiswa */
    public MataKuliah[] getDaftarMatkul() {
        return daftarMataKuliah;
    }
}