package assignments.assignment3;

public class Makanan {
    private String nama;

    private long harga;

    /** Constructor class Makanan.
     * 
     * @param nama = nama makanan
     * @param harga = harga makanan
     */
    public Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    @Override
    public String toString() {
        return this.nama;
    }

    /** @return harga makanan */
    public long getHarga() {
        return this.harga;
    }
}