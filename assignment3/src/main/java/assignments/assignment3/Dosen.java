package assignments.assignment3;

public class Dosen extends ElemenFasilkom {

    private MataKuliah mataKuliah;

    /** Constructor kelas Dosen, mengikuti constructor super dari ElemenFasilkom dengan tipe "Dosen" */
    Dosen(String nama) {
        super(nama, "Dosen");
    }

    /**Dipakai untuk mengassign objek Dosen ke objek MataKuliah yang sesuai.
     * Jika objek mataKuliah belum mempunyai dosen pengajar dan dosen belum mengajar mata kuliah apapun, bisa di add.
     * Jika objek dosen sedang mengajar mata kuliah lain, masuk ke else if pertama.
     * Jika objek mataKuliah sedang mengajar mata kuliah lain, masuk ke else if kedua.
     * @param mataKuliah = objek mataKuliah yang akan diassign ke dosen.
     */
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        if (mataKuliah.canAddDosen() && this.mataKuliah == null) {                                          //Jika this.mataKuliah == null -> dosen belum mengajar suatu mata kuliah
            this.mataKuliah = mataKuliah;
            mataKuliah.addDosen(this);
            System.out.println(this.nama + " mengajar mata kuliah " + mataKuliah);
        } else if (this.mataKuliah != null) {                                                               //Jika this.mataKuliah != null -> dosen sedang mengajar suatu mata kuliah
            System.out.println("[DITOLAK] " + this + " sudah mengajar mata kuliah " + this.mataKuliah);
        } else if (!mataKuliah.canAddDosen()) {                                                             //mataKuliah.canAddDosen() return true jika belum ada dosen yang mengajar mata kuliah tersebut
            System.out.println("[DITOLAK] " + mataKuliah + " sudah memiliki dosen pengajar");
        }
    }

    /** Dipakai untuk drop mata kuliah yang diassign ke objek dosen */
    public void dropMataKuliah() {
        if (mataKuliah != null) {                                                                           //mataKuliah != null -> dosen sedang mengajar suatu mata kuliah
            System.out.println(nama + " berhenti mengajar " + mataKuliah);
            mataKuliah.dropDosen();
            mataKuliah = null;
        } else {                                                                                            //mataKuliah == null -> dosen tidak mengajar mata kuliah apa-apa
            System.out.println("[DITOLAK] " + nama + " sedang tidak mengajar mata kuliah apapun");
        }
    }

    /** @return variabel mataKuliah yang dimiliki objek. (mataKuliah yang sedang diajar oleh dosen) */
    public MataKuliah getMataKuliah() {
        return mataKuliah;
    }

    @Override
    public String toString() {
        return nama;
    }
}