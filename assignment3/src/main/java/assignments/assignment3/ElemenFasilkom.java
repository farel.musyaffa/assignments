package assignments.assignment3;

abstract class ElemenFasilkom {
    String tipe;
    
    String nama;

    int friendship;

    ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    /** Constructor kelas abstract ElemenFasilkom.
     * @param nama = Nama dari elemen fasilkom.
     * @param tipe = Tipe dari elemen fasilkom. Class Dosen -> tipe "Dosen", Class ElemenKantin -> tipe "ElemenKantin", class Mahasiswa -> tipe "Mahasiswa".
     */
    public ElemenFasilkom(String nama, String tipe) {
        this.nama = nama;
        this.tipe = tipe;
    }

    /** Method interaksi antar elemen fasilkom.
     * Jika parameter elemenFasilkom ada di array telahMenyapa, menyapa tidak dilakukan.
     * Jika parameter elemenFasilkom tidak ada di array telahMenyapa, add objek elemenFasilkom ke array telahMenyapa
     * dan add objek this pada array telahMenyapa milik elemenFasilkom
     * @param elemenFasilkom = elemen fasilkom yang akan disapa.
     */
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        if (cekTelahMenyapa(elemenFasilkom)) {
            System.out.println("[DITOLAK] " + this + " telah menyapa " + elemenFasilkom + " hari ini");
        } else {
            for (int i = 0; i < telahMenyapa.length; i++) {
                if (telahMenyapa[i] == null) {                                                              //Cari index pada array yang kosong
                    telahMenyapa[i] = elemenFasilkom;                                                       //Assign index kosong dengan objek yang disapa
                    for (int j = 0; j < elemenFasilkom.getTelahMenyapa().length; j++) {                     //Assign objek this pada array telahMenyapa milik elemenFasilkom
                        if (elemenFasilkom.getTelahMenyapa()[j] == null) {
                            elemenFasilkom.getTelahMenyapa()[j] = this;
                            break;
                        }
                    }
                    System.out.println(this + " menyapa dengan " + elemenFasilkom);
                    break;
                }
            }
        }
    }

    /** Mereset array telahMenyapa milik objek. */
    public void resetMenyapa() {
        for (int i = 0; i < telahMenyapa.length; i++) {
            telahMenyapa[i] = null;
        }
    }

    /** ElemenFasilkom membeli makanan milik ElemenFasilkom tipe ElemenKantin.
     * 
     * @param pembeli = ElemenFasilkom yang akan membeli makanan. Bisa bertipe apa saja, tidak boleh sama dengan objek penjual.
     * @param penjual = ElemenFasilkom yang akan menjual makanan. Harus bertipe ElemenKantin.
     * @param namaMakanan = Nama makanan yang akan dibeli. Harus ada di daftarMakanan milik ElemenKantin penjual
     */
    public static void membeliMakanan(ElemenFasilkom pembeli, ElemenFasilkom penjual, String namaMakanan) {
        if (penjual.tipe.equals("ElemenKantin")) {                                                                      //Jika objek penjual bertipe ElemenKantin
            ElemenKantin penjualMakanan = (ElemenKantin) penjual;                                                       //Cast objek ElemenFasilkom penjual menjadi objek ElemenKantin
            boolean found = false;                                                                                      //Variabel boolean untuk mennetukan String yang akan diprint
            Makanan[] daftarMakanan = penjualMakanan.getDaftarMakanan();
            for (int i = 0; i < daftarMakanan.length; i++) {
                if (daftarMakanan[i] != null && daftarMakanan[i].toString().equals(namaMakanan)) {                      //Cari makanan yang ingin dibeli pada daftarMakanan objek ElemenKantin
                    System.out.println(pembeli.toString() + " berhasil membeli " + namaMakanan + " seharga " + daftarMakanan[i].getHarga());
                    pembeli.setFriendship(1);
                    penjual.setFriendship(1);
                    found = true;
                    break;
                }
            }
            if (!found) {                                                                                               //found hanya bernilai false jika makanan yang ingin dibeli tidak ada di daftarMakanan
                System.out.println("[DITOLAK] " + penjual.toString() + " tidak menjual " + namaMakanan);
            }
        } else {                                                                                                        //Jika objek penjual bukan bertipe ElemenKantin
            System.out.println("[DITOLAK] elemen penjual bukan bertipe ElemenKantin");
        }
    }

    /** Menghitung jumlah objek yang ada di array telahMenyapa() .
     * @return berapa banyak orang yang telah disapa pada hari tersebut.
    */
    public int countMenyapa() {
        int count = 0;
        for (int i = 0; i < telahMenyapa.length; i++) {
            if (telahMenyapa[i] != null) {                                                                              //telahMenyapa[i] != null -> ada suatu objek yang telah disapa
                count++;
            }
        }
        return count;
    }

    /** Menghitung nilai friendship baru milik objek ElemenFasilkom.
     * this.friendship += param friendship
     * @param friendship = Nilai yang akan ditambahkan dengan nilai friendship yang dimiliki objek. Bisa bernilai negatif.
     */
    public void setFriendship(int friendship) {
        if (this.friendship + friendship < 0) {
            this.friendship = 0;
        } else if (this.friendship + friendship > 100) {
            this.friendship = 100;
        } else {
            this.friendship += friendship;
        }
    }

    /** @return array telahMenyapa milik ElemenFasilkom */
    public ElemenFasilkom[] getTelahMenyapa() {
        return telahMenyapa;
    }

    /** @return nilai friendship */
    public int getFriendship() {
        return friendship;
    }

    @Override
    public String toString() {
        return nama;
    }

    /** @return tipe.
     *  Class Dosen -> tipe "Dosen". Class ElemenKantin -> tipe "ElemenKantin". Class Mahasiswa -> tipe "Mahasiswa".
     */
    public String getTipe() {
        return tipe;
    }

    /** Mengecek apakah objek this telah menyapa objek elemen pada parameter.
     * 
     * @param elemen = elemen yang ingin dicek apakah sudah disapa atau belum
     * @return true jika sudah disapa, false jika belum disapa
     */
    public boolean cekTelahMenyapa(ElemenFasilkom elemen) {
        for (int i = 0; i < telahMenyapa.length; i++) {
            if (telahMenyapa[i] == elemen) {
                return true;
            }
        }
        return false;
    }
}