package assignments.assignment2;

public class Mahasiswa {
    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private String[] masalahIRS = new String[100];
    private int totalSKS = 0;
    private String namaMahasiswa;
    private String jurusanMahasiswa;
    private long npmMahasiswa;

    public Mahasiswa(String nama, long npm){
        namaMahasiswa = nama;
        npmMahasiswa = npm;
    }
    
    public void addMatkul(MataKuliah mataKuliah){
        int findOpenIndex = 0;
        while (daftarMataKuliah[findOpenIndex] != null) {               //Sama dengan addMahasiswa pada MataKuliah.java. Bedanya disini method dijamin memiliki index yang valid karena sudah dihandle di SistemAkademik
            findOpenIndex++;
        }
        daftarMataKuliah[findOpenIndex] = mataKuliah;
    }

    public void dropMatkul(MataKuliah mataKuliah){
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] != null && daftarMataKuliah[i].toString().equals(mataKuliah.toString())) {                  //Jika daftarMataKuliah[i] == null dan kita mencoba untuk menggunakan method toString(), akan terjadi error.
                daftarMataKuliah[i] = null;
            }
        }
    }


    public void cekIRS() {
        /* TODO: implementasikan kode Anda di sini */
        if (totalSKS > 24) {                                                                                                   //Jika total SKS lebih dari 24, tambah string tersebut pada masalahIRS index ke 0.
            masalahIRS[0] = "SKS yang diambil lebih dari 24.";
        }
        
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] == null) {
                continue;
                //break;
            } else if (getJurusan().equals("Ilmu Komputer")) {
                switch (daftarMataKuliah[i].getKode()){
                    case "SI" -> masalahIRS[i + 1] = "Mata kuliah " + daftarMataKuliah[i].toString() + " tidak dapat diambil jurusan " + jurusanMahasiswa + ".";            //Jika jurusan Ilmu Komputer dan mengambil mata kuliah SI, maka tambahkan string pada masalahIRS
                }
            } else if (getJurusan().equals("Sistem Informasi")) {
                switch (daftarMataKuliah[i].getKode()) {
                    case "IK" -> masalahIRS[i + 1] = "Mata kuliah " + daftarMataKuliah[i].toString() + " tidak dapat diambil jurusan " + jurusanMahasiswa + ".";            //Jika jurusan Sistem Informasi dan mengambil mata kuliah IK, maka tambah string tersebut pada masalahIRS
                }
            }
        }

        boolean masalah = false;
        for (int i = 0, j = 1; i < masalahIRS.length; i++) {
            if (masalahIRS[i] == null) {                                                            
                continue;
            } else {                                                                                    //Jika ada masalah, print tiap masalahIRS tersebut
                System.out.println(j + ". " + masalahIRS[i]);
                j++;
                masalah = true;                                                                         //ubah variabel masalah menjadi true, yang nantinya akan menentukan string yang di print
            }
        }
        if (!masalah) {                                                                                 //Variabel masalah hanya akan false jika tidak ada string di dalam array masalahIRS, yang artinya tidak ada masalah IRS.
            System.out.println("IRS tidak bermasalah.");
        }
        masalahIRS = new String[100];
    }

    public String toString() {
        return namaMahasiswa;
    }

    public long getNPM() {
        return npmMahasiswa;
    }

    public int getJumlahMatkulKosong() {
        int total = 10;
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] == null) {                                                          
                continue;
                //break;
            } else {                                                                                    //Jika daftarMataKuliah[i] != null, ada objek MataKuliah, sehingga kurangi total dengan 1
                total--;
            }
        }
        return total;
    }

    public String getJurusan() {
        String strNPM = "" + npmMahasiswa;
        switch (strNPM.substring(2,4)) {
            case "01" -> jurusanMahasiswa = "Ilmu Komputer";                                            //Jika substring 2,4 == 01, maka mahasiswa tersebut memiliki jurusan Ilmu Komputer
            case "02" -> jurusanMahasiswa = "Sistem Informasi";                                         //Jika substring 2,4 == 02, maka mahasiswa tersebut memiliki jurusan Sistem Informasi
            default -> jurusanMahasiswa = "n";
        }
        return jurusanMahasiswa;
    }

    public MataKuliah[] getMataKuliah() {
        return daftarMataKuliah;
    }

    public int jumlahSKS() {
        totalSKS = 0;
        for (int i  = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] == null) {
                continue;
                //break;
            } else {
                totalSKS += daftarMataKuliah[i].getSKS();                                           //Jika daftarMataKuliah[i] != null, berarti ada objek matkul. Ambil jumlah sks matkul tersebut dan tambahkan dengan total sks
            }
        }
        return totalSKS;
    }

    public boolean cekMatkul(String matkul) {                                                       //Mengecek apakah matkul tersebut sudah diambil oleh mahasiswa atau belum.
        boolean sudahDiambil = false;
        for (int i = 0; i < daftarMataKuliah.length; i++) {
            if (daftarMataKuliah[i] != null && matkul.equals(daftarMataKuliah[i].toString())) {
                sudahDiambil = true;
            }
        }
        return sudahDiambil;
    }
}
