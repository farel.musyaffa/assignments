package assignments.assignment2;

public class MataKuliah {
    private String kodeMatkul;
    private String namaMatkul;
    private int sksMatkul;
    private int kapasitasMatkul;
    private Mahasiswa[] daftarMahasiswa;

    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        kodeMatkul = kode;
        namaMatkul = nama;
        sksMatkul = sks;
        kapasitasMatkul = kapasitas;
        daftarMahasiswa = new Mahasiswa[kapasitas];
    }


    public void addMahasiswa(Mahasiswa mahasiswa) {
        int findOpenIndex = 0;                                                                                          //Cari dulu index keberapa yang kosong,
        while (findOpenIndex < daftarMahasiswa.length && daftarMahasiswa[findOpenIndex] != null) {                      //Jika variabel findOpenIndex >= length daftarMahasiswa, maka tidak ada index yang kosong
            findOpenIndex++;                                                                                            //Jika daftarMahasiswa[findOpenIndex] != null, maka index tersebut sudah ada objek yang menempati
        }
        if (findOpenIndex < daftarMahasiswa.length) {                                                                   //Jika tidak ada index yang kosong, jangan lakukan apa apa, karena handle jika kapasitas penuh ada di SistemAkademik.
            daftarMahasiswa[findOpenIndex] = mahasiswa;                                                                 //Jika ada index kosong, isi index tersebut dengan objek mahasiswa.
        }
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] != null && daftarMahasiswa[i].getNPM() == mahasiswa.getNPM()) {                      //Setelah drop matkul, akan ada index yang null, dan jika kita mencoba untuk melakukan getNPM pada null menghasilkan error.
                daftarMahasiswa[i] = null;                                                                              //Jika ditemukan mahasiswa dengan npm yang sama, ubah daftarmahasiswa pada index tersebut menjadi null.
            }
        }
    }

    public String toString() {
        if (namaMatkul != null) {                                                                                       
            return namaMatkul;
        } else {
            return null;
        }
    }

    public int getSKS() {
        return sksMatkul;
    }

    public String getKode() {
        return kodeMatkul;
    }

    public int getJumlahMahasiswa() {
        int totalMahasiswa = 0;
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] == null) {                                                                           //Jika daftarMahasiswa[i] == null, berarti jumlah mahasiswa i-1, sehingga harus dilakukan break
                continue;
                //break;
            } else {
                totalMahasiswa++;                                                                                       //Jika daftarMahasiswa[i] != null, berarti pada index tersebut ada objek mahasiswa, tambahkan 1 pada counter
            }
        }
        return totalMahasiswa;
    }

    public int getKapasitas() {
        return kapasitasMatkul;
    }

    public boolean cekKapasitas() {
        int banyakMahasiswa = 0;
        for (int i = 0 ; i < daftarMahasiswa.length; i++) {                                                             
            if (daftarMahasiswa[i] != null) {                                                                          //Sebenarnya sama dengan method getJumlahMahasiswa
                banyakMahasiswa++;
            } else {
                continue;
                //break;
            }
        }
        if (banyakMahasiswa >= kapasitasMatkul) {                                                                       //Jika banyakMahasiswa lebih dari atau sama dengan kapasitas matkul, return false yang berarti matkul sudah penuh
            return false;
        } else {
            return true;
        }
    }

    public void getDaftarMahasiswa() {
        boolean adaMahasiswa = false;                                                                                   //Asumsikan mata kuliah masih belum ada yang mengambil,
        for (int i = 0, j = 1; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i] == null) {
                continue;
            } else {
                System.out.println((j) + ". " + daftarMahasiswa[i]);                                                    //Jika ada yang mengambil, print nama mahasiswa tersebut dan variabel adaMahasiswa = true
                adaMahasiswa = true;
                j++;
            }
        }
        if (!adaMahasiswa) {                                                                                            //Variabel ada mahasiswa hanya akan false jika method tidak menemukan objek mahasiswa pada daftarMahasiswa, yang berarti belum ada mahasiswa yang mengambil mata kuliah tersebut.
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini.");
        }
    }
}
