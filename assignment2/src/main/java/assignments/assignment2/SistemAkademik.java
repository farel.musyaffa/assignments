package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    private Mahasiswa getMahasiswa(long npm) {                          //cek di daftar mahasiswa. Jika tidak ada, return null
        for (int i = 0; i < 100; i++) {
            if (daftarMahasiswa[i] == null) {
                break;
            } else if (daftarMahasiswa[i].getNPM() == npm) {
                return daftarMahasiswa[i];
            }
        }
        return null;
    }

    private MataKuliah getMataKuliah(String namaMataKuliah) {           //Cek di daftar mata kuliah. jika tidak ada, return null
        for (int i = 0; i < 100; i++) {
            if (daftarMataKuliah[i] == null) {
                break;
            } else if (daftarMataKuliah[i].toString().equals(namaMataKuliah)) {
                return daftarMataKuliah[i];
            }
        }
        return null;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        boolean inputError = true;                                                                      //Untuk while loop pada try catch
        long npm = 0;
        while (inputError) {                                                                            //Asumsikan ada error pada input, yaitu numberformatexception
            try {
                npm = Long.parseLong(input.nextLine());
                inputError = false;                                                                     //Jika input valid, inputerror = false sehingga program lanjut berjalan
            } catch (NumberFormatException e) {
                System.out.println("Input yang dimasukkan tidak valid. \n");
                System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");           //Jika input membuahkan error, masukkan lagi input hingga valid.
            }
        }
        inputError = true;                                                                              //Agar nantinya bisa dipakai untuk try catch input kedepannya

        boolean matkulSudahDiambil = false;                                                             //Set variabel boolean untuk memnentukan apakah matkul sudah diambil
        Mahasiswa targetMahasiswa;                                                                      //Alokasi memori untuk variabel tipe Mahasiswa
        targetMahasiswa = getMahasiswa(npm);                                                            //assign variabel tersebut ke objek dari class mahasiswa yang tepat melalui method getMahasiswa diatas.

        if (targetMahasiswa != null) {                                                                  //Method getMahasiswa akan return null jika tidak ada npm pada database. Jika tidak null, npm valid.
            System.out.print("Banyaknya Matkul yang Ditambah: ");
            int banyakMatkul = 0;
            while (inputError) {
                try {
                    banyakMatkul = Integer.parseInt(input.nextLine());
                    inputError = false;
                } catch (NumberFormatException e) {
                    System.out.println("Inpput yang dimasukkan tidak valid. \n");
                    System.out.print("Banyak Matkul yang Ditambah: ");
                }
            }
            System.out.println("Masukkan nama matkul yang ditambah");
            for(int i=0; i<banyakMatkul; i++) {
                System.out.print("Nama matakuliah " + (i+1) + " : ");
                String namaMataKuliah = input.nextLine();

                matkulSudahDiambil = targetMahasiswa.cekMatkul(namaMataKuliah);                         //method cekMatkul pada class Mahasiswa akan mereturn boolean false dan true berdasarkan apakah matkul yang ingin diambil sudah diambil sebelumnya.
                
                if (getMataKuliah(namaMataKuliah) == null) {                                            //Jika method getMataKuliah == null, berarti tidak ada mata kuliah dengan nama tersebut di database.
                    System.out.println("(" + namaMataKuliah +") [DITOLAK] Mata kuliah tidak terdaftar dalam sistem.");
                } else {
                    if (targetMahasiswa.getJumlahMatkulKosong() != 0 && (!matkulSudahDiambil) && getMataKuliah(namaMataKuliah).cekKapasitas()) {            //Jika mahasiswa belum mengambil 10 matkul, matkul belum diambil dan matakuliah masih ada kapasitas kosong
                        targetMahasiswa.addMatkul(getMataKuliah(namaMataKuliah));                                                                       
                        getMataKuliah(namaMataKuliah).addMahasiswa(targetMahasiswa);
                    } else if (matkulSudahDiambil) {                                                                                                        //Jika matkul sudah diambil
                        System.out.println("[DITOLAK] " + namaMataKuliah + " telah diambil sebelumnya.");
                    } else if (!getMataKuliah(namaMataKuliah).cekKapasitas()) {                                                                             //Jika matkul sudah penuh kapasitasnya
                        System.out.println("[DITOLAK] " + namaMataKuliah + " telah penuh kapasitasnya.");
                    } else if (targetMahasiswa.getJumlahMatkulKosong() == 0) {                                                                              //Jika jumlah matkul yang sudah diambil lebih dari 10
                        System.out.println("[DITOLAK] Maksimal mata kuliah yang dapat diambil hanya 10.");
                        break;
                    }
                }
            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        } else {                                                                                                                                            //Jika npm yang dimasukkan tidak ada di database
            System.out.println("[DITOLAK] Tidak ada mahasiswa dengan NPM " + npm + " di database.");
        }
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = 0;
        boolean inputError = true;
        while (inputError) {
            try {
                npm = Long.parseLong(input.nextLine());
                inputError = false;
            } catch (NumberFormatException e) {
                System.out.println("Input yang dimasukkan tidak valid. \n");
                System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
            }
        }
        inputError = true;
        Mahasiswa targetMahasiswa;                                  //Alokasi vairabel targetMahasiswa sebagai objek dari class Mahasiswa
        MataKuliah targetMatkul;                                    //Alokasi variabel targetMatkul sebagai objek dari MataKuliah
        targetMahasiswa = getMahasiswa(npm);                        //method getMahasiswa akan mereturn null jika npm tidak ada di daftar.

        if (targetMahasiswa == null) {                              //Jika npm tidak ada di daftar
            System.out.println("[DITOLAK] Tidak ada mahasiswa dengan NPM " + npm + " di database.");
        } else if (targetMahasiswa.getJumlahMatkulKosong() == 10) {                                     //Jika belum ada matkul yang diambil oleh mahasiswa
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
        } else {
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = 0;
            while (inputError) {
                try {
                    banyakMatkul = Integer.parseInt(input.nextLine());
                    inputError = false;
                } catch (NumberFormatException e) {
                    System.out.println("Inpput yang dimasukkan tidak valid. \n");
                    System.out.print("Banyak Matkul yang Ditambah: ");
                }
            }
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + (i+1) + " : ");
                String namaMataKuliah = input.nextLine();
                targetMatkul = getMataKuliah(namaMataKuliah);                                           //getMataKuliah akan mereturn objek dari class MataKuliah jika ada, dan null jika tidak ada di daftar
                if (!targetMahasiswa.cekMatkul(namaMataKuliah)) {                                       //cekMatkul akan mereturn boolean true jika matkul sudah diambil. Jika matkul belum diambil, tidak bisa drop.
                    System.out.println("[DITOLAK] " + namaMataKuliah + " belum pernah diambil.");
                } else {
                    targetMahasiswa.dropMatkul(targetMatkul);
                    targetMatkul.dropMahasiswa(targetMahasiswa);
                }

            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        }
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = 0;
        boolean inputError = true;
        while (inputError) {
            try {
                npm = Long.parseLong(input.nextLine());
                inputError = false;
            } catch (NumberFormatException e) {
                System.out.println("Input yang dimasukkan tidak valid. \n");
                System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
            }
        }
        inputError = true;

        boolean validNPM = false;
        String namaTargetMahasiswa = "";
        Mahasiswa targetMahasiswa = new Mahasiswa("", 0);
        for (int i = 0; i < daftarMahasiswa.length; i++) {
            if (daftarMahasiswa[i].getNPM() == npm) {
                namaTargetMahasiswa = daftarMahasiswa[i].toString();            //method Mahasiswa.toString() akan mereturn variabel local namaMahasiswa
                targetMahasiswa = daftarMahasiswa[i];
                validNPM = true;                                                
                break;
            }
        }

        if (targetMahasiswa.getJurusan().equals("n")) {                         //getJurusan() akan mereturn string Sistem Informasi atau Ilmu Komputer, dan n jika npm tidak valid
            System.out.println("[DITOLAK] Jurusan tidak valid.");
            validNPM = false;
        }

        if (validNPM) {
            System.out.println("\n--------------------------RINGKASAN--------------------------\n");
            System.out.println("Nama: " + namaTargetMahasiswa);
            System.out.println("NPM: " + npm);
            System.out.println("Jurusan: " + targetMahasiswa.getJurusan());
            System.out.println("Daftar Mata Kuliah: ");

            if (targetMahasiswa.getJumlahMatkulKosong() == 10) {                //Jika belum ada matkul yang diambil
                System.out.println("Belum ada mata kuliah yang diambil.");
            } else{
                for (int i = 0; i < 10 - targetMahasiswa.getJumlahMatkulKosong(); i++) {                //Untuk semua matkul pada daftarMatkul mahasiswa,
                    System.out.println((1+i) + ". " + targetMahasiswa.getMataKuliah()[i]);              //Print tiap matkul yang diambil mahasiswa.
                }
            }

            System.out.println("Total SKS: " + targetMahasiswa.jumlahSKS());
            
            System.out.println("Hasil Pengecekan IRS:");
            targetMahasiswa.cekIRS();                                                                   //method cekIRS() akan memprint hasil pengecekan IRS yang pengecekannya sendiri dilakukan di dalam method tersebut.
        } else {
            System.out.println("Tidak ada mahasiswa dengan npm" + npm + ".");
        }
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah targetMataKuliah;
        targetMataKuliah = getMataKuliah(namaMataKuliah);

        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + targetMataKuliah.toString());
        System.out.println("Kode: " + targetMataKuliah.getKode());
        System.out.println("SKS: " + targetMataKuliah.getSKS());
        System.out.println("Jumlah mahasiswa: " + targetMataKuliah.getJumlahMahasiswa());
        System.out.println("Kapasitas: " + targetMataKuliah.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");

        targetMataKuliah.getDaftarMahasiswa();
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }

    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = 0;
        
        boolean inputError = true;
        while (inputError) {
            try {
                banyakMatkul = Integer.parseInt(input.nextLine());
                inputError = false;
            } catch (NumberFormatException e) {
                System.out.println("Input yang dimasukkan tidak valid.");
                System.out.print("Banyaknya Matkul di Fasilkom: \n");
            }
        }
        
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            daftarMataKuliah[i] = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);             //Mengappend objek class MataKuliah ke dalam array daftarMatkul
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            daftarMahasiswa[i] = new Mahasiswa(dataMahasiswa[0], npm);                                      //Mengappend objek class Mahasiswa ke dalam array daftarMahasiswa
        }

        daftarMenu();
        input.close();
    }
    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }
}
