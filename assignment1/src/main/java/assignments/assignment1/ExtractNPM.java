package assignments.assignment1;

import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem
    
    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */
    public static int validateNum(long npm) {
        String stringNPM = "" + npm;                                                                                    //Mengubah long NPM menjadi string
        int[] arrNPM = new int[stringNPM.length()-1];                                                                   //Membuat sebuah array dengan index sebanyak digit NPM
        for (int i = 0; i < stringNPM.length()-1; i++) {                                                                //Mengisi tiap array tersebut dengan tiap digit NPM
            arrNPM[i] = Integer.parseInt(stringNPM.substring(i, i + 1));
        }

        int res = 0;
        for (int i = 0; i < stringNPM.length()/2; i++) {                                                                //Loop untuk menjumlahkan tiap hasil kali index digit NPM
            res += (arrNPM[i] * arrNPM[(stringNPM.length()-2)-i]);
        }

        while (res >= 10) {                                                                                             //loop, selama hasil penambahan tersebut lebih dari dua digit
            String stringRes = "" + res;                                                                                //Sama seperti NPM pada diawal, hasil penambahan diubah menjadi string agar bisa di indexing dan dimasukkan kedalam array
            int[] arrRes = new int[stringRes.length()];
            for (int i = 0; i < stringRes.length(); i++) {
                arrRes[i] = Integer.parseInt(stringRes.substring(i, i + 1));
            }
            res = 0;                                                                                                     //Setelah semua digit hasil berhasil dipindahkan ke array baru, variabel res akan dipakai ulang dengan mengubah valuenya menjadi 0 terlebih dahulu
            for (int i = 0; i < stringRes.length(); i++) {                                                              //Menambahkan semua digit dari hasil.
                res += arrRes[i];
            }
        }
        return res;                                                                                                      //Jika hasilnya satu digit, kembalikan value tersebut.
    }

    public static int age(long npm) {
        String stringNPM = "" + npm;                                                                                    //Ubah ke string agar bisa di indexing
        int entryYear = Integer.parseInt(stringNPM.substring(0, 2)) + 2_000;
        int birthYear = Integer.parseInt(stringNPM.substring(8, 12));
        return entryYear - birthYear;                                                                                   //Mengembalikan umur dari pemilik NPM
    }

    public static boolean validate(long npm) {
        boolean valid = true;
        String stringNPM = "" + npm;                                                                                    //Ubah NPM ke string agar bisa di indexing
        if (age(npm) < 15) {                                                                                            //Jika umur saat masuk kurang dari 15, asumsi tidak valid
            valid = false;
        }
        if (validateNum(npm) != Integer.parseInt((stringNPM).substring(stringNPM.length()-1))) {                        //Menggunakan method validateNum yang akan mengembalikan hasil dari proses validasi,
            valid = false;                                                                                              //Jika hasil sama dengan digit terakhir, valid
        }                                                                                                               //Jika tidak, maka tidak valid
        return valid;
    }

    public static String extract(long npm) {
        String stringNPM = "" + npm;                                                                                    //Ubah NPM ke string agar bisa di indexing
        String tahunMasuk = "20" + stringNPM.substring(0,2);
        String jurusan = stringNPM.substring(2,4);
        switch (jurusan) {                                                                                              //Switch case untuk menentukan jurusan, overwrite isi dari jurusan yang sebelumnya
            case "01" -> jurusan = "Ilmu Komputer";                                                                     //Tidak ada default case, karena kode hanya akan sampai ke extract
            case "02" -> jurusan = "Sistem Informasi";                                                                  //jika NPM yang dimasukkan valid, jadi sudah pasti
            case "03" -> jurusan = "Teknologi Informasi";                                                               //string yang dimasukkan ke switch case ini ada di casenya
            case "11" -> jurusan = "Teknik Telekomunikasi";
            case "12" -> jurusan = "Teknik Elektro";
        }
        String tanggalLahir = stringNPM.substring(4, 6) + "-"
                + stringNPM.substring(6,8) + "-"
                + stringNPM.substring(8,12);
        String res = "Tahun masuk: " + tahunMasuk + "\n"
                + "Jurusan: " + jurusan + "\n"
                + "Tanggal Lahir: " + tanggalLahir;
        return res;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }
            if (validate(npm)) {                                                                                        //Method validate akan mengembalikan variabel boolean "valid".
                String res = extract(npm);                                                                              //Jika valid, extract informasi yang dibutuhkan dan simpan dalam res
                System.out.println(res);                                                                                //Print res
            } else {
                System.out.println("NPM tidak valid!");
            }
        }
        input.close();
    }
}